
$(function(){
  $('.submitBtn').click(function(){
    let errors= false;
    let email = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    $('.errors').remove();
    if($('#name').val() == "" || $('#phone').val() == "" || $('#workplace').val() == ""  || $('#position').val() == "" ){
      $('#name').addClass('error')
      $('#phone').addClass('error')
      $('#workplace').addClass('error')
      $('#position').addClass('error')
      $('#name').after('<span class="errors">Bu field doldurulmalıdır</span>');
      $('#phone').after('<span class="errors">Bu field doldurulmalıdır</span>');
      $('#workplace').after('<span class="errors">Bu field doldurulmalıdır</span>');
      $('#position').after('<span class="errors">Bu field doldurulmalıdır</span>');

      errors=true;
    }
    $(".form-input").focus(function() { 
      $(this).next('span').remove();
      $(this).removeClass('error');  
    });
    if(errors == false){
  return true;
    }else{        
      return false;
  }})
})
  


$("#phone").inputmask({
  mask: '+\\9\\94 99 999-99-99',
    showMaskOnHover: false,
});



jQuery(document).ready(function() {
  jQuery(document).on("click", ".map-button", function(e) {
      e.preventDefault();
      var latLng = jQuery(this).attr("data-latLng");			
      initialize(latLng);
  });
  $(document).ready(function(){
    $(".map-button").click(function(){
      $(".map-button").not(this).removeClass("active");
      $(this).addClass("active");
  }); });
  
  function initialize(latLng) {
      latLng = latLng.split(",")
      var mapOptions = {
          center: new google.maps.LatLng(latLng[0],latLng[1]),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          streetViewControl: false,
          styles: 
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#212121"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#212121"
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#181818"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1b1b1b"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#2c2c2c"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8a8a8a"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#373737"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#3c3c3c"
            }
          ]
        },
        {
          "featureType": "road.highway.controlled_access",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#4e4e4e"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#000000"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3d3d3d"
            }
          ]
        }
      ]
      };
      var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  }
});	



// const mapInit = (locationArr) => {
//   if (!locationArr.length) {
//       return false
//   }

//   const centered = new google.maps.LatLng(locationArr[0][0], locationArr[0][1]);
//   const mapOptions = {
//       center: centered,
//       zoom: 15,
//       mapTypeId: google.maps.MapTypeId.ROADMAP,
//       disableDefaultUI: true,
//       streetViewControl: false,
//       styles: 
//       [
//         {
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#212121"
//             }
//           ]
//         },
//         {
//           "elementType": "labels.icon",
//           "stylers": [
//             {
//               "visibility": "off"
//             }
//           ]
//         },
//         {
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#757575"
//             }
//           ]
//         },
//         {
//           "elementType": "labels.text.stroke",
//           "stylers": [
//             {
//               "color": "#212121"
//             }
//           ]
//         },
//         {
//           "featureType": "administrative",
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#757575"
//             }
//           ]
//         },
//         {
//           "featureType": "administrative.country",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#9e9e9e"
//             }
//           ]
//         },
//         {
//           "featureType": "administrative.land_parcel",
//           "stylers": [
//             {
//               "visibility": "off"
//             }
//           ]
//         },
//         {
//           "featureType": "administrative.locality",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#bdbdbd"
//             }
//           ]
//         },
//         {
//           "featureType": "poi",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#757575"
//             }
//           ]
//         },
//         {
//           "featureType": "poi.park",
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#181818"
//             }
//           ]
//         },
//         {
//           "featureType": "poi.park",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#616161"
//             }
//           ]
//         },
//         {
//           "featureType": "poi.park",
//           "elementType": "labels.text.stroke",
//           "stylers": [
//             {
//               "color": "#1b1b1b"
//             }
//           ]
//         },
//         {
//           "featureType": "road",
//           "elementType": "geometry.fill",
//           "stylers": [
//             {
//               "color": "#2c2c2c"
//             }
//           ]
//         },
//         {
//           "featureType": "road",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#8a8a8a"
//             }
//           ]
//         },
//         {
//           "featureType": "road.arterial",
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#373737"
//             }
//           ]
//         },
//         {
//           "featureType": "road.highway",
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#3c3c3c"
//             }
//           ]
//         },
//         {
//           "featureType": "road.highway.controlled_access",
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#4e4e4e"
//             }
//           ]
//         },
//         {
//           "featureType": "road.local",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#616161"
//             }
//           ]
//         },
//         {
//           "featureType": "transit",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#757575"
//             }
//           ]
//         },
//         {
//           "featureType": "water",
//           "elementType": "geometry",
//           "stylers": [
//             {
//               "color": "#000000"
//             }
//           ]
//         },
//         {
//           "featureType": "water",
//           "elementType": "labels.text.fill",
//           "stylers": [
//             {
//               "color": "#3d3d3d"
//             }
//           ]
//         }
//       ]
      
//   };
//   const mapCanvas = document.getElementById("map");
//   const map = new google.maps.Map(mapCanvas, mapOptions);
//   const infoWindow = new google.maps.InfoWindow();
//   let marker, i;
//   const baseURL = window.location.origin;
//   for (i = 0; i < locationArr.length; i++) {
//       marker = new google.maps.Marker({
//           position: new google.maps.LatLng(locationArr[i][0], locationArr[i][1]),
//           animation: google.maps.Animation.DROP,
//           map: map,
//           icon: {
//             url: '../img/',
//             scaledSize: new google.maps.Size(95, 95),
//         }
        
//       });
      
//   }
// };
// const resizeMarker = (marker, map, pos = {}, isClicked = false) => {
//   const oldIcon = marker.getIcon();
//   const sizeX = oldIcon.scaledSize.width;
//   const sizeY = oldIcon.scaledSize.height;
//   const newIcon = {url: oldIcon.url, scaledSize: new google.maps.Size(sizeX + 50, sizeY + 50)};
//   if (!isClicked) {
//       marker.setIcon(newIcon);
//   }
//   if (Object.keys(pos).length) {
//       map.panTo(pos);
//   }
// };
// const getLocations = (selector) => {
//   const locations = document.querySelectorAll(selector);
//   if (!locations.length) {
//       return []
//   }
//   return Array.from(locations).map(el => [Number(el.dataset.lat), Number(el.dataset.lng), el.dataset.icon]);
// };

// mapInit(getLocations('.branch'));


// function myMap() {
//   var mapProp= {
//     center:new google.maps.LatLng(51.508742,-0.120850),
//     zoom:5,
    // styles: 
    // [
    //   {
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#212121"
    //       }
    //     ]
    //   },
    //   {
    //     "elementType": "labels.icon",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#757575"
    //       }
    //     ]
    //   },
    //   {
    //     "elementType": "labels.text.stroke",
    //     "stylers": [
    //       {
    //         "color": "#212121"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "administrative",
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#757575"
    //       },
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "administrative.country",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#9e9e9e"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "administrative.land_parcel",
    //     "elementType": "labels",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "administrative.locality",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#bdbdbd"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "poi",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "poi",
    //     "elementType": "labels.text",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "poi",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#757575"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "poi.park",
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#181818"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "poi.park",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#616161"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "poi.park",
    //     "elementType": "labels.text.stroke",
    //     "stylers": [
    //       {
    //         "color": "#1b1b1b"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road",
    //     "elementType": "geometry.fill",
    //     "stylers": [
    //       {
    //         "color": "#2c2c2c"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road",
    //     "elementType": "labels.icon",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#8a8a8a"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road.arterial",
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#373737"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road.highway",
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#3c3c3c"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road.highway.controlled_access",
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#4e4e4e"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road.local",
    //     "elementType": "labels",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "road.local",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#616161"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "transit",
    //     "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "transit",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#757575"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "water",
    //     "elementType": "geometry",
    //     "stylers": [
    //       {
    //         "color": "#000000"
    //       }
    //     ]
    //   },
    //   {
    //     "featureType": "water",
    //     "elementType": "labels.text.fill",
    //     "stylers": [
    //       {
    //         "color": "#3d3d3d"
    //       }
    //     ]
    //   }
    // ]
  // };
//   var map = new google.maps.Map(document.getElementById("map"),mapProp);
//   }
   
//   myMap();